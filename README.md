# Programmation-Enfants

Je dois animer 10 scéances Peri-scolaires pour faire aimer et apprendre a des eleves de CM2 la programmation. 
Je vous partage ici mes scéances (et aussi je mettrais les difficultées rencontrées) 

Si vous souhaitez interagir, merci de me logguer des issues

je publierai régulièrement ici ainsi que via twitter et au final qq part


Quelques liens intéressants : 

 * Scratch : 
    * https://scratch.mit.edu/
    * Scratch X : https://scratchx.org/
 * WorkBench 
    * https://edu.workbencheducation.com/toolbox/programming
    * https://edu.workbencheducation.com/cwists/category
 * Blocky : 
    *  https://blockly-games.appspot.com/
    *  https://developers.google.com/blockly/
 * Thymio :
    *  https://www.thymio.org/fr/