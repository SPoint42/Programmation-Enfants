# Etat des lieux 
Le groupe est composé de 18 eleves. Le niveau est assez disparatre....de plus certains éléves sont des amis de mon fils (cela n'aidera pas forcément)

Les ordinateurs sont tous dans un état non forcé "bons" et surtout assez anciens. 

L'acces internet fonctionne plutot bien.
Ce sont des scéances de 45mns (théoriques), mais entre l'arrivée et la mise en place il faut bien faire attention

----
# Premières difficultés

Apres avoir fourni le matériel, il faut faire la police....Ne pas laisser les enfants faire ce qu'il veulent et les "cadrer"

---- 
# Suite a la première Scéance

Les enfants sont excités par le sujet et n'arrivent pas à se concentrer, je décide donc de commencer la prochaine scéance par une programmation "papier". Pour cela je vais imprimer les différents blocs de scratch et les platifier pour leur faire faire de l'assemblage et ensuite reproduire ce qu'ils ont assemblé

----
# Suite a la seconde scéance : 

Je décide de finalement faire autrement. Je vais leur "ecrire" avec eux le code scratch sur un ordinateurs et leur faire reproduire. Ils ont du mal a reproduire le papier(surement que je m'y suis mal pris pour les blocs, j'aurai peut etre du les diviser plus => A voir a la 4eme scéance)


----
# Suite a la 3eme scéance . 
Je suis arrivé en avance pour préparer la salle (démarrage des ordis et pointage sur Scratch). Cela s'avere nécessaire car l'excitation des scéances fait qu'ils perdent 5/10 mns pour se trouver sur scratch....

La programmation avec eux sur un écran semble meilleure que les blocs papier. Mais j'ai moins de participation du groupe, ils sont plus en mode "je gobe". Il va falloir réfléchir a autre chose pour plus d'interaction.

