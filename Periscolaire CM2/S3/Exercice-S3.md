﻿----
# Déplacer un lutin et le faire interagir avec un autre lutin

Tout d'abord il va falloir faire en sorte que lorsque le chat touche la souris alors la souris d'enfuie. Pour cela il est nécessaire d'ajouter un élément d'interaction entre les deux lutins (Sprite)

Il existe un bloc permettant d'interagir entre deux lutins

![](media/interaction1.pnd.png)

Notre programme (pour la souris) devient alors : 

![](media/interaction2.png)


----
# Ajouter un score

La gestion du score va se faire via un compteur que nous allons créer. Un compteur se nomme ici une "variable". Nous lui donnerons le nom de "Score"

![](media/score1.png)



Il va falloir ensuite à chaque fois ou le chat touche la souris, ajouter 1 au score.

![](media/score2.png)


Puis remettre le score du Chat à 0 a chaque fois ou le programme redémarre. 

![](media/score3.png)


Cela nous donnera  : 

Programme Chat : 

![](media/chatscore1.png)

Programme Souris

![](media/scoresouris1.png)


---- 
# Arreter le programme quand nous avons atteint le score maximum

Pour cela nous allons utiliser la capacité du "Si Alors, sinon" et les blocs dses opérateurs mathématiques : 

![](media/bloc%20score.png)


De la manière suivante : 

![](media/scoresi.png)

Ce qui nous donnera le programme suivant : 

![](media/finjeu.png)