﻿## Ajouter un second lutin et le déplacer aléatoirement
L'ajout du second lutin passe pas l'ajout d'une souris qui va se déplacer aléatoirement. 

L'ajout de la souris : 

![](media/ajoutsouris1.png)

![](media/ajoutsouris2.png)

Puis on ajoute le code du déplacement aléatoire :

![](media/aleatoire1.png)

A ce stade la souris se déplace de manière aléatoire et le chat se déplace en fonction de la touche appuyée. 

Il va convenir de faire maintenant le lien entre les deux morceaux du programme

