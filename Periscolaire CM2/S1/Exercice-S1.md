%
% Premiers pas avec Scratch
% (c) 2019 AppsecFR
----


# Découverte de l’interface de manière didactique
# Premiers programmes
Scratch se trouve à l'URL http://scratch.mit.edu
Pour les besoins de ce Periscolaire, j'ai créer un compte Google mail pour s'enregistrer et sauvegarder les differents programmes

----
Nous allons développer un jeu de chat et de souris. A chaque fois que le chat réussi à se déplacer sur la souris, alors nous gagnons un point puis la souris se déplace de nouveau à un endroit aléatoire de l'écran

----
# Inserer un lutin et le deplacer
## Insérer un lutin (sprite)
## Faire réagir un lutin sur événement
Nous allons faire réagir le lutin sur l'appui de chacune des fleches. 
Pour cela il est nécessaire de comprendre le fonctionnement des boucles. 
Le programme final est le suivant : 
![](media/deplacementChat.png)


