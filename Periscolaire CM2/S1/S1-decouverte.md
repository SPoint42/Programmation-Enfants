% Programmation - Périscolaire
% Découverte de l'environnement
% (c) AppSecFR 2019

-----

# Séquence 1 - Découverte de l'environnement

* Programme des 10 scéances 
* Quelques rappels sur l'ordinateur
* La programmation c'est quoi ?
* Découverte de Scratch
* Exercice 1 : 
  * Déplacement d'un personnage sur l'écran
  
  
-----

# Ce que nous allons essayer de faire sur ces 10 ateliers 

* Réaliser des petits programmes simples
* Réaliser un jeu de ping-pong des années  1980
* Programmer un robot pour effectuer des déplacements simples
* Réaliser un labyrinthe et déplacer le robot dans le labyrinthe
* Vous faire découvrir la face cachée de l'informatique
* Epatez vos parents :)
* **Etre attentif pour bien comprendre ce que vais raconter !** 

-----

# Rappels sur l'ordinateur 

-----

# Histoire du calcul 

* L’impulsion première du calcul a été donné par les commerçants qui  avaient besoin de pouvoir échanger facilement leurs biens avec  d’autres individus.

* On peut distinguer trois périodes principales de l’histoire	du calcul:
  * de la préhistoire aux années 1930,
  * de 1930 à 1950,
  * après 1950.

-----
# La préhistoire

* De la préhistoire à 1930, l’homme emploie différents moyens pour  arriver à ses fins ;; certains de ces moyens sont encore utilisés de  nos jours.
* Tout d’abord le corps est employé : main, doigts, …. Tout ceci permet  des transactions qui se font sous les tuniques en se touchant les  doigts permettant	de donner un chiffre.
* Cette méthode montre très rapidement ses limites, car trop difficilement  applicable en dehors de transaction de faible montant (on était limité  a 10 doigts).
* Ensuite l’homme va utiliser les entailles sur le bois ou sur les os. Cela  lui permet alors de retenir plus de chiffres, mais toujours par de  l’aider à calculer !!


-----
# Le problème du Zero : 

* Pendant très longtemps (jusqu’à plusieurs  siècles après JC) l’homme ne connaît pas  le chiffre zéro.
* Le zéro ne vient à l’esprit des indiens et  des arabes que 2/3 siècles après JC.
* Par contre il n’arrive en Europe dans des  manuscrits qu’au alentours du 9ème siècle.

-----
## Astérix et Esteban

# Les quipus 

Certaines civilisations se mettent alors à employer des cordelettes comportant des  nœuds. Les indiens Incas, utilisent toujours ce mécanisme (le Quipus). Ce  mécanisme permet de compter rapidement des grands nombres.

Exemple :
* 3  nœuds réunissant 5 cordelettes == 30 000
* 3 nœuds réunissant 2 cordelettes == 30
* 3 nœuds sur 1 cordelette == 3

## Les abaques 

A l’époque des romains apparaissent les abaques.

L’abaque est une planche de bois composée de colonnes parallèles creusées. Chaque  colonne représente une dizaine.
1ère colonne == les unités
2ème colonne == les dizaines
3ème colonne == les centaines
–	…….

Des cailloux disposés dans les colonnes permettent d’indiquer les chiffres.

Ceci permet d’avoir une machine a compter portable très bon marché et très facile à  transporter !

----

# Les premiers ordinateurs

-----

# Scratch

## L’interface scratch
L’ interface de scratch comporte plusieurs parties :
1. Le menu global.
2. Les blocs.
3. L espace de programmation.
4. L espace d execution.

## les LUTINS
Les lutins sont des personnages que chacun peut créer par une image ou tout simplement prendre ceux fournis par scratch. Le lutin par défaut de scratch est le chat 

## scènes
Les scenes correspondent aux fond visuel de chacun des programmes 

## blocs
Un bloc permet un peu comme avec des legos de construire ce que l’on souhaite.
Il existe différentes catégories de blocs. A chaque bloc est associé une couleur et des propriétés spécifiques ;
- Déplacement ; bleu
- Visuels et looks ; violet
- Contrôles ; orange et jaune 
- Évènements ; 
- Opérateurs ; vert
- Groupe de blocs ; rose 

