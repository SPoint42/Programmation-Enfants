# Le chamboule-tout makey-makey 

Lors de la dernière fete de l Ecole j avais propose un stand de jeux robots/electro. Mais personne n en a voulu, on voulait des jeux plus “classiques”.

J’ ai eu une idée cette année de faire un chamboule-tout a base de makeymakey. 

Le but étant de marier les elements “historiques” avec un potentiel atelier de scratch.

Chacune des boites ayant une valeur différente cela permet de rajouter via scratch un compteur de score (si vous avez un Micro:bit) vous pouvez meme jouer de l’affichage
