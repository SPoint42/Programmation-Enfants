# Réalisation des boites conductrices

- Percer les boites avec un petit foret (genre 3) pour pouvoir attacher le fil conducteur dessus
- Attacher le fil conducteur sur la boite; éventuellement il est possible de le souder

# Realisation des plaques conductrices

- Découper des morceaux de cartons d une taille inférieur au diamètre des boites. 
- Découper des morceaux de feuilles d aluminium que l on colle sur ces morceaux de cartons
- Relier chacun des morceaux de cartons comportant les feuilles d aluminium ensemble. Pour cela on peut faire une découpe de carton sur lequel on colle un autre bout d aluminium
- **L’idée est que le courant passe par la boite et cet element via les pinces crocodiles **

# Realisation du câblage
- Brancher la terre du makeymakey a une des plaques conductrices assemblées
- Brancher chacun des fils des boites a une pince crocodile du makeymakey.


